READ-ME

falta fazer: 

calculo do erro global (esta somente somando os locais)

bibliografia:

1 - https://en.wikipedia.org/wiki/Linear_multistep_method

2 - https://chasqueweb.ufrgs.br/~carolina.manica/cap10.pdf

3 - http://www.math.ucla.edu/~yanovsky/Teaching/Math151B/handouts/GramSchmidt.pdf

4 - http://www.math.ttu.edu/~rvenkata/Papers/opt_timestep_ijnam.pdf

5 - https://en.wikipedia.org/wiki/Adaptive_stepsize

Calculo de erros:

6 - https://www.computer.org/csdl/proceedings/afips/1968/5071/00/50710467.pdf

7 - http://mathfaculty.fullerton.edu/mathews/n2003/abmmethod/adamsbashforthproof.pdf

9 - https://www.fc.unesp.br/Home/Departamentos/Matematica/revistacqd2228/v07a02-comparacao-entre-metodos-numericos.pdf

10 - https://en.wikipedia.org/wiki/Truncation_error_(numerical_integration)

11 - https://www-m2.ma.tum.de/foswiki/pub/M2/Allgemeines/NumProg2/notes3.pdf

13 - https://math.okstate.edu/people/binegar/4513-F98/4513-l24.pdf

QR decomposition

8 - https://en.wikipedia.org/wiki/QR_decomposition

12 - Iterative Methods for Computing Eigenvalues and Eigenvectors - Maysum Panju


start("test1", 0, 0.1, 3, 0.000005, "(exp(1)^X)*Y", "exp(1)^((exp(1)^(X))-1)")

start("test2", 0, 0.1, 10, 0.000005, "sin(X)*Y", "exp(1)^(1-cos(X))")